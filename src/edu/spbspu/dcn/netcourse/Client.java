package edu.spbspu.dcn.netcourse;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

/**
 * Created by grihon07 on 12.09.2014.
 */
public class Client {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket(args[0], Integer.parseInt(args[1]));
            //System.out.println("local port: "+socket.getLocalPort());

            Scanner in = new Scanner(System.in);
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            String cons = in.nextLine();
            while(!cons.equals("exit")) {

                dataOutputStream.writeUTF(cons);
                cons = in.nextLine();
            }
            socket.close();
        } catch (SocketException e){
            System.out.println("Server not found");
        } catch (IOException e) {
           // e.printStackTrace();
        }
    }
}
