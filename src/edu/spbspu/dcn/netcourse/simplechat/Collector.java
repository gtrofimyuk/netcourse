package edu.spbspu.dcn.netcourse.simplechat;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created by grihon07 on 16.12.2014.
 */
public class Collector {
    private Thread cleaner;
    private TreeSet<String> logins;
    private LinkedList<SimpleMessage> history;
    private HashMap<Integer, String> idToLogin;
    private final Object lock;

    public Collector(final int timeout){
        this.lock = new Object();
        this.logins = new TreeSet<>();
        this.history = new LinkedList<>();
        this.idToLogin = new HashMap<>();

        cleaner = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (lock) {
                        int size = history.size();
                        int loginSize = logins.size();
                        if (loginSize > 0 && size > loginSize * 10) {
                            int loopCond = (int) ((float) size * 0.85);
                            for (int i = 0; i < loopCond; ++i) {
                                history.removeFirst();
                            }
                        }
                    }
                    try {
                        Thread.sleep(timeout);
                    } catch (InterruptedException e) {
                        //
                    }
                }
            }
        });

    }

    public void startCleaner(){
        cleaner.start();
    }

    public boolean addLogin(String login, int id){
        boolean res;
        synchronized (lock) {
            res = logins.add(login);
        }
        if(res){
            synchronized (lock) {
                idToLogin.put(id, login);
            }
        }
        return res;
    }

    public void addMessageToHistory(SimpleMessage simpleMessage){
        synchronized (lock){
            history.add(simpleMessage);
        }
    }

    public ArrayList<String> getMessages(LocalDateTime lastUpdated){
        ArrayList<String> msgList = new ArrayList<String>();
        synchronized (lock){
            msgList.addAll(history.stream().filter(ms -> ms.date.isAfter(lastUpdated)).map
                    (ms -> "[" + ms.date.getHour() +":" + ms.date.getMinute() + "]" + idToLogin.get(ms.id) +
                            ":> " + ms.message).collect(Collectors.toList()));
        }
        return msgList;
    }

    public void deleteLoginInfo(int id, String login){
        synchronized (lock){
            idToLogin.remove(id);
            logins.remove(login);
        }
    }
}
