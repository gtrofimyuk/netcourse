package edu.spbspu.dcn.netcourse.simplechat;

import edu.spbspu.dcn.netcourse.netutils.MessageHandler;
import edu.spbspu.dcn.netcourse.netutils.MessageHandlerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeSet;

/**
 * Created by grihon07 on 31.10.2014.
 */
public class CMHFImpl implements MessageHandlerFactory {

    private Collector collector;
    private volatile int id = 0;

    public CMHFImpl(final int timeout){
        collector = new Collector(timeout);
        collector.startCleaner();
    }

    @Override
    public MessageHandler create() {
        return new ChatMessageHandlerImpl(collector, id++);
    }
}
