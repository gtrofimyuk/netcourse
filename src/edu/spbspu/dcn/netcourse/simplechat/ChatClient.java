package edu.spbspu.dcn.netcourse.simplechat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.util.Scanner;

/**
 * Created by grihon07 on 31.10.2014.
 */
public class ChatClient {
    private static String login;

    public static void main(String[] args) {
        try {
            Socket socket = new Socket(args[0], Integer.parseInt(args[1]));

            Scanner in = new Scanner(System.in);
            final DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            final DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            System.out.println("Please enter you login. Must not contain ':' symbol");

            login = in.nextLine();
            boolean isLoginCorrect = false;
            while(!isLoginCorrect) {
                if (login.contains(":")) {
                    System.out.println("Login contain ':'. Please enter new login");
                    login = in.nextLine();
                } else {
                    isLoginCorrect = true;
                }
            }

            dataOutputStream.writeUTF("addlogin: "+login);
            boolean isAuthorized = false;
            while(!isAuthorized){
                isAuthorized = Boolean.valueOf(dataInputStream.readUTF());
                if(!isAuthorized){
                    System.out.println("This login already exist. Please, enter new login");
                    login = in.nextLine();
                    isLoginCorrect = false;

                    while(!isLoginCorrect) {
                        if (login.contains(":")) {
                            System.out.println("Login contain ':'. Please enter new login");
                            login = in.nextLine();
                        } else {
                            isLoginCorrect = true;
                        }
                    }

                    dataOutputStream.writeUTF("addlogin: "+login);
                }
            }
            System.out.println("Welcome, "+login+"!");

            final Thread updater = new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean isRunning = true;
                    while(isRunning) {
                        try {
                            dataOutputStream.writeUTF("update: ");
                            Thread.sleep(200);
                        } catch(SocketException | InterruptedException e){
                            isRunning = false;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            Thread reader = new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean isRunning = true;
                    while(isRunning) {
                        int a = 6;
                        try {
                            String msg =  dataInputStream.readUTF();
                            char[] received = msg.toCharArray();
                            int start;
                            if(received[5] == ']'){
                                start = 5;
                            }else if(received[6] == ']'){
                                start = 6;
                            }
                            else start = 7;
                            for(int i = start; i < received.length; ++i){
                                if(received[i] == ':'){
                                    a = i;
                                    break;
                                }
                            }
                            if(!login.equals(msg.substring(start+1,a))){
                                System.out.println(msg);
                            }
                        } catch (IOException e) {
                            isRunning = false;
                        }
                    }
                }
            });
            updater.start();
            reader.start();
            String cons = in.nextLine();
            while(!cons.equals("exit") && socket.isConnected()) {
                dataOutputStream.writeUTF("message: "+cons);
                cons = in.nextLine();
            }

            updater.interrupt();
            reader.interrupt();
            socket.close();

        } catch (SocketException e){
            System.out.println("Server not found");
        } catch (IOException e) {
            // e.printStackTrace();
        }
    }
}
