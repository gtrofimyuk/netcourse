package edu.spbspu.dcn.netcourse.simplechat;

import edu.spbspu.dcn.netcourse.concurrentutils.Channel;
import edu.spbspu.dcn.netcourse.concurrentutils.ThreadPool;
import edu.spbspu.dcn.netcourse.netutils.Dispatcher;
import edu.spbspu.dcn.netcourse.netutils.Host;
import edu.spbspu.dcn.netcourse.netutils.MessageHandlerFactory;

/**
 * Created by grihon07 on 31.10.2014.
 */
public class ChatServer {

    //TODO: finally
    //TODO: check synchronized
    public static void main(String[] args) {
        int port = Integer.parseInt(args[0]);
        int maxConnections = Integer.parseInt(args[1]);
        int timeout = Integer.parseInt(args[2]);
        final Channel channel = new Channel();
        final ThreadPool threadPool = new ThreadPool(1, maxConnections, 2);

        MessageHandlerFactory messageHandlerFactory = new CMHFImpl(timeout);

        final Dispatcher dispatcher = new Dispatcher(channel, threadPool, messageHandlerFactory);
        final Host host = new Host(port, channel);


        Runtime.getRuntime().addShutdownHook(new Thread(){
            public void run() {
                host.stop();
                threadPool.stop();
                dispatcher.stop();
            }
        });

        dispatcher.start();
        host.start();
    }
}
