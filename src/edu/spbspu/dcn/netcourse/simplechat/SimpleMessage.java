package edu.spbspu.dcn.netcourse.simplechat;

import java.time.LocalDateTime;

/**
 * Created by grihon07 on 07.11.2014.
 */
public class SimpleMessage {
    public final String message;
    public final int id;
    public final LocalDateTime date;

    public SimpleMessage(String message, int id, LocalDateTime date){
        this.message = message;
        this.id = id;
        this.date = date;
    }
}
