package edu.spbspu.dcn.netcourse.simplechat;

import edu.spbspu.dcn.netcourse.netutils.MessageHandler;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Created by grihon07 on 31.10.2014.
 */
public class ChatMessageHandlerImpl implements MessageHandler {

    private final Collector collector;
    private final int id;
    private LocalDateTime lastUpdated;
    private String login;

    public ChatMessageHandlerImpl(Collector collector, int id){
        this.collector = collector;
        this.id = id;
    }

    public ArrayList<String> handle(String message) {

        int j = getSeparatorPosition(message);

        ArrayList<String> response = new ArrayList<String>();

        if (j == -1) {
            response.add("Error: wrong request");
            return response;
        }

        String command = message.substring(0, j);
        String text = null;
        if (j + 1 < message.length())
            text = message.substring(j + 2);


        switch (command) {
            case "addlogin":
                boolean res = collector.addLogin(text, id);
                response.add(Boolean.toString(res));
                if (res) {
                    lastUpdated = LocalDateTime.now();
                    login = text;
                }
                return response;
            case "message":
                collector.addMessageToHistory(new SimpleMessage(text, id, LocalDateTime.now()));
                break;
            case "update":
                ArrayList<String> msgList = collector.getMessages(lastUpdated);
                lastUpdated = LocalDateTime.now();
                return msgList;
            default:
                return null;
        }
        return null;
    }

    @Override
    public void deleteSessionInfo() {
        collector.deleteLoginInfo(id, login);
    }

    public int getSeparatorPosition(String message){
        if(message == null){
            return -1;
        }
        char[] msg = message.toCharArray();
        int j = -1;
        for (int i = 0; i < msg.length; ++i) {
            if (msg[i] == ':') {
                if (i != msg.length - 1) {
                    if (msg[i + 1] == ' ') {
                        j = i;
                        break;
                    }
                }
            }
        }
        return j;
    }
}
