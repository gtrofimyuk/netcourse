package edu.spbspu.dcn.netcourse;

import edu.spbspu.dcn.netcourse.netutils.MessageHandler;
import edu.spbspu.dcn.netcourse.netutils.MessageHandlerFactory;

/**
 * Created by grihon07 on 10.10.2014.
 */
public class MHFImpl implements MessageHandlerFactory{

    @Override
    public MessageHandler create() {
        return new MessageHandlerImpl();
    }
}
