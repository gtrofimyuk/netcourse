package edu.spbspu.dcn.netcourse.netutils;

import edu.spbspu.dcn.netcourse.concurrentutils.Channel;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by grihon07 on 12.09.2014.
 */
public class Host implements Runnable{
    private final int port;
    ServerSocket serverSocket;
    private boolean isRunning = false;
    private final Channel channel;
    private final Thread thread;

    public Host(int port, Channel channel){
        this.port = port;
        this.channel = channel;
        thread = new Thread(this, "Host");
    }

    public void start() {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        thread.start();
        isRunning = true;
    }

    @Override
    public void run() {
        while (isRunning) {
            try {
                channel.put(serverSocket.accept());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void stop(){
        System.out.println("closing host: server socket");
        isRunning = false;
        if(!serverSocket.isClosed()){
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}


