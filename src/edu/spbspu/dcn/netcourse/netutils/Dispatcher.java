package edu.spbspu.dcn.netcourse.netutils;

import edu.spbspu.dcn.netcourse.concurrentutils.Channel;
import edu.spbspu.dcn.netcourse.concurrentutils.ThreadPool;

import java.io.IOException;

/**
 * Created by grihon07 on 19.09.2014.
 */
public class Dispatcher implements Runnable{

    private final Channel channel;
    private final ThreadPool threadPool;
    private final MessageHandlerFactory messageHandlerFactory;
    private boolean isRunning = false;
    private final Thread thread;

    public Dispatcher(Channel channel, ThreadPool threadPool, MessageHandlerFactory messageHandlerFactory){
        this.channel = channel;
        this.threadPool = threadPool;
        this.messageHandlerFactory = messageHandlerFactory;
        thread = new Thread(this, "dispatcher");
    }

    public void run(){
        while(isRunning){
            threadPool.executeTask(new Session((java.net.Socket) channel.get(), messageHandlerFactory.create()));
        }
    }

    public void start(){
        isRunning = true;
        thread.start();
    }

    public void stop(){
        isRunning = false;
        while(!channel.isEmpty()){
            try {
                ((java.net.Socket) channel.get()).close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        thread.interrupt();
    }
}
