package edu.spbspu.dcn.netcourse.netutils;

import edu.spbspu.dcn.netcourse.concurrentutils.Closeable;
import javafx.util.Pair;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by grihon07 on 12.09.2014.
 */
public class Session implements Closeable{
    private boolean isRunning;
    private final Socket socket;
    private final MessageHandler messageHandler;

    public Session(Socket socket, MessageHandler messageHandler) {
        this.socket = socket;
        this.messageHandler = messageHandler;
        isRunning = true;
    }

    @Override
    public void run() {
        DataInputStream dataInputStream = null;
        DataOutputStream dataOutputStream = null;
        //connectionCount++;
        try {
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());

            while (isRunning) {
                ArrayList<String> msg = messageHandler.handle(dataInputStream.readUTF());
                if(msg != null) {
                    for (String aMsg : msg) {
                        dataOutputStream.writeUTF(aMsg);
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("Client disconnected");
        }
        finally {
            messageHandler.deleteSessionInfo();
            close();
        }
    }

    @Override
    public void close() {
        isRunning = false;
        if(!socket.isClosed()) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
