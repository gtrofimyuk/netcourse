package edu.spbspu.dcn.netcourse.netutils;

/**
 * Created by grihon07 on 10.10.2014.
 */
public interface MessageHandlerFactory {
    MessageHandler create();
}
