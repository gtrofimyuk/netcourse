package edu.spbspu.dcn.netcourse.netutils;

import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by grihon07 on 10.10.2014.
 */
public interface MessageHandler {
    public ArrayList<String> handle(String message);
    public void deleteSessionInfo();
}
