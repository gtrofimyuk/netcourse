package edu.spbspu.dcn.netcourse.udp.sender;

import java.util.Vector;

/**
 * Created by grihon07 on 11.12.2014.
 */
public class Timer implements Runnable{

    private SlidingWindowController slidingWindowController;
    private boolean isRunning = true;

    public Timer(SlidingWindowController slidingWindowController){
        this.slidingWindowController = slidingWindowController;
    }
    @Override
    public void run() {
        while(isRunning){
            slidingWindowController.doTimerIteration();
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stop(){
        isRunning = false;
    }
}
