package edu.spbspu.dcn.netcourse;

import edu.spbspu.dcn.netcourse.concurrentutils.Channel;
import edu.spbspu.dcn.netcourse.concurrentutils.ThreadPool;
import edu.spbspu.dcn.netcourse.netutils.Dispatcher;
import edu.spbspu.dcn.netcourse.netutils.Host;
import edu.spbspu.dcn.netcourse.netutils.MessageHandlerFactory;

/**
 * Created by grihon07 on 09.10.2014.
 */
public class Server {
    public static void main(String[] args) {
        int port = Integer.parseInt(args[0]);
        int maxConnections = Integer.parseInt(args[1]);
        String app = args[2];
        final Channel channel = new Channel();
        final ThreadPool threadPool = new ThreadPool(1, maxConnections, 2);

        Class cl = null;
        try {
            cl = Class.forName("edu.spbspu.dcn.netcourse."+app);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Object obj = null;
        try {
            if (cl != null) {
                obj = cl.newInstance();
            }
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        MessageHandlerFactory messageHandlerFactory = (MessageHandlerFactory) obj;

        final Host host = new Host(port, channel);
        final Dispatcher dispatcher = new Dispatcher(channel, threadPool, messageHandlerFactory);

        Runtime.getRuntime().addShutdownHook(new Thread(){
            public void run() {
                host.stop();
                threadPool.stop();
                dispatcher.stop();
            }
        });

        host.start();
        dispatcher.start();
    }
}
